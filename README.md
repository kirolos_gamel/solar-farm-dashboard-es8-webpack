# 🚀 Welcome to solar farm dashboard project!

This project has been created using **webpack scaffold**, you can now run

```
npm run build
```

or

```
yarn build
```

to bundle your application

# Developer: Kirolos Ibrahim

# Description of project:

```
A dashboard to monitor solar farm energy production. Solar farms consist of 30 solar panels, has a server that is able to provide voltage and wattage per solar panel. REST forecast data service is provided separately.
:Requirements
Dashboard has to tell the user the total energy in kW produced at the moment (aggregated from all panels). Dashboard has to list all panels with their IDs in a grid, where each cell has values of current voltage and wattage output. The dashboard should indicate basic weather forecast information like solar activity "Visible Diffuse Downward Solar Flux" in watts per sq uare meter and % of sky cloud coverage (100% — clear sky). The UI should have a responsive layout - adapt to desktop, tablet and smartphone screens. The dashboard should be able to poll a server every 10 sec. to update voltage, wattage information and poll REST weather data service every 5 min. Use git while developing and push to a public repository (eg. Github, Bitbucket).
APIs and sample data examples:
Cloud coverage http://api.planetos.com/v1/datasets/bom_access-g_global_40km/point?lon=-50.5&lat=49.5&apikey=<apikey>&var=av_ttl _cld&csv=true&count=50 Solar activity http://api.planetos.com/v1/datasets/bom_access-g_global_40km/point?lon=-50.5&lat=49.5&apikey=<apikey>&var=av_swsf cdown&csv=true&count=50 see details about this API in the docs http://docs.planetos.com/#datasets-id-point Sample data for fake data generator could be derived from service like this one http://pvoutput.org/live.jsp
```
#Technologies:

```
HTML - CSS - CSS3 - BOOTSTRAP FRAMEWORK - JAVASCRIPT VANILLA ES6 - WEBPACK 
```

