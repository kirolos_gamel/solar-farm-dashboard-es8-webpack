"use strict";
import dataSolarPanels from './dataSolarPanels';

import axios from 'axios';

// CSS
import '../css/bootstrap.min.css';
import '../css/custom.css';
//images
import img from '../img/solar-panel.jpg';
import loaderImg from '../img/loader.svg';

let fakeDataSet = new dataSolarPanels();

const pullWindForecast = async (param) => {
    let divData = document.querySelector('#forecastData');
    if (param === 1) {
        divData.style.display = 'none';
        let loader = document.querySelector('#loader');
        loader.innerHTML = `<img class="mx-auto" src="${loaderImg}" />`;
    }

    try {

        let response = await axios.get('http://api.planetos.com/v1/datasets/noaa_ww3_global_1.25x1d/point', {
            params: {
                lon: "-50.5",
                lat: "49.5",
                apikey: "480d7c6fdae84b97bea9af699c93ca71"
            }
        });

        console.log(response);

        if (response.status == 200 && response.statusText == "OK") {
            let speed = document.querySelector('#wind-sp');
            let drBSur = document.querySelector('#dr-b-sur');
            let drOSur = document.querySelector('#dr-of-sur');
            let prWaMb = document.querySelector('#pr-wa-mb');
            let prWaDs = document.querySelector('#pr-wa-ds');

            let length = response.data.entries.length;
            for (let i = 0; i < length; i++) {
                if (i === 0) {
                    console.log(response.data.entries[0].data);
                    speed.innerHTML = Math.trunc(response.data.entries[0].data.Wind_speed_surface);
                    drBSur.innerHTML = Math.trunc(response.data.entries[0].data.Wind_direction_from_which_blowing_surface);
                    drOSur.innerHTML = Math.trunc(response.data.entries[0].data.Direction_of_wind_waves_surface);
                    prWaMb.innerHTML = Math.trunc(response.data.entries[0].data.Primary_wave_mean_period_surface);
                    prWaDs.innerHTML = Math.trunc(response.data.entries[0].data.Primary_wave_direction_surface);
                    break;
                }
            }
            if (param === 1) {
                loader.style.display = 'none';
                divData.style.display = 'flex';
            }
        }

    } catch (err) {
        alert("Error in connection with Api.");
        console.log(err)
    }

}


//function to change data every 10 seconds
const pullFakeData = () => {
    let dataSet = fakeDataSet.createData();

    let lengthDataSet = dataSet[0].length;

    let vot;
    let watt;
    let kw;
    let idObj;

    for (let i = 0; i < lengthDataSet; i++) {
        for (let property in dataSet[0][i]) {

            if (property === "id") {
                idObj = dataSet[0][i].id;
            }
            if (property === "vottage") {
                vot = document.querySelector('#vot-' + idObj);
                vot.innerHTML = dataSet[0][i].vottage;
            }

            if (property === "watts") {
                watt = document.querySelector('#wat-' + idObj);
                watt.innerHTML = dataSet[0][i].watts;
            }
            if (property === "kw") {
                kw = document.querySelector('#kw-' + idObj);
                kw.innerHTML = dataSet[0][i].kw;
            }
        }
    }

    let totalPower = document.querySelector('#total-now');
    totalPower.innerHTML = dataSet[1] + " KW";

}

// Document ready event
document.addEventListener('DOMContentLoaded', function () {
    pullWindForecast(1);
    let dataSet = fakeDataSet.createData();
    console.log(dataSet);
    // Get the element you want to add your new element inside
    let target = document.querySelector('#solar-panels');
    let lengthDataSet = dataSet[0].length;
    for (let z = 0; z < lengthDataSet; z++) {
        // Create the new element
        let divPanel = document.createElement('div');
        divPanel.className = 'col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 mb-4';
        let divInnerPanel = `<div class="card shadow mb-4">
            <div class="card-header py-3"> `;
        for (let property in dataSet[0][z]) {
            //console.log(property);
            if (property === "id") {
                divInnerPanel += `
                <h6 class="m-0 font-weight-bold text-primary text-center"> Solar Panel ${dataSet[0][z].id}</h6>
                </div>
                <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="${img}" alt="">
                </div>
                `;
            }
            if (property === "vottage") {
                divInnerPanel += `
                    <p class="text-center" > Vottage: <span id="vot-${dataSet[0][z].id}"> ${dataSet[0][z].vottage} </span> 
                `;
            }

            if (property === "watts") {
                divInnerPanel += `
                  / Watts: <span id="wat-${dataSet[0][z].id}" > ${dataSet[0][z].watts} </span>  </p>
                `;


            }
            if (property === "kw") {
                divInnerPanel += `
                     <p class="text-center" > KW: <span id="kw-${dataSet[0][z].id}" > ${dataSet[0][z].kw} </span> </p>
                    </div>
                </div>
              `;
            }
        }

        // Add content to the new element  
        divPanel.innerHTML = divInnerPanel;
        target.appendChild(divPanel);

    }

    let totalPower = document.querySelector('#total-now');
    totalPower.innerHTML = dataSet[1] + " KW";



});



document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
        setInterval(pullFakeData, 10000);
        setInterval(pullWindForecast, 300000, 0);
    }

};