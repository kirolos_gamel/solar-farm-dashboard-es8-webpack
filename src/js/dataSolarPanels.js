"use strict";
class dataSolarPanels {
  fakeData = new Array();
  //create fake data array of objects randomize values
  createData = () => {
    let totalPower = 0;
    let result = [];

    this.fakeData = [];

    let obj = {};
    for (let i = 1; i <= 30; i++) {
      obj = {};
      obj = {
        "id": i,
        "vottage": Math.floor(Math.random() * 500) + 1,
        "watts": Math.floor(Math.random() * 500) + 1,
        "kw": Math.floor(Math.random() * 500) + 1
      };
      this.fakeData = [...this.fakeData, obj];
      totalPower += obj.kw;
    }

    result = [this.fakeData, totalPower];
    return result;

  }

}

export default dataSolarPanels;